<?php

namespace App\Entity;

use App\Repository\WalletRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: WalletRepository::class)]
class Wallet
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'integer')]
    private $amount;


    #[ORM\ManyToMany(targetEntity: Crypto::class, inversedBy: 'wallets')]
    private $Crypto_in_wallet;

    #[ORM\OneToOne(inversedBy: 'wallet', targetEntity: User::class, cascade: ['persist', 'remove'])]
    private $user_wallet;


    public function __construct()
    {
        $this->relation = new ArrayCollection();
        $this->Crypto_in_wallet = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAmount(): ?int
    {
        return $this->amount;
    }

    public function setAmount(int $amount): self
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * @return Collection<int, Crypto>
     */
    public function getCryptoInWallet(): Collection
    {
        return $this->Crypto_in_wallet;
    }

    public function addCryptoInWallet(Crypto $cryptoInWallet): self
    {
        if (!$this->Crypto_in_wallet->contains($cryptoInWallet)) {
            $this->Crypto_in_wallet[] = $cryptoInWallet;
        }

        return $this;
    }

    public function removeCryptoInWallet(Crypto $cryptoInWallet): self
    {
        $this->Crypto_in_wallet->removeElement($cryptoInWallet);

        return $this;
    }

    public function getUserWallet(): ?User
    {
        return $this->user_wallet;
    }

    public function setUserWallet(?User $user_wallet): self
    {
        $this->user_wallet = $user_wallet;

        return $this;
    }

}